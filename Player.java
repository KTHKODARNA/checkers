import java.util.*;

public class Player {
    /**
     * Performs a move
     *
     * @param pState
     *            the current state of the board
     * @param pDue
     *            time before which we must have returned
     * @return the next state the board is in after our move
     */
    
    /**
     * COLOR
     *
     * Red : 1
     * White : 0
     * 
     */
    
    int maxDepth = 7;
    int depth = 0;
    int inf = 99999;
    int[] safe = {3, 4, 11, 12, 19, 20, 27, 28};

    public GameState play(final GameState pState, final Deadline pDue) {
        Vector<GameState> lNextStates = new Vector<GameState>();
        pState.findPossibleMoves(lNextStates);

        if (lNextStates.size() == 0) {
            // Must play "pass" move if there are no other moves possible.
            return new GameState(pState, new Move());
        }

        // Här börjar Player
        int bestMove = -inf;
        int index = 0;

        for(int i = 0; i < lNextStates.size(); i++) {
            int move = -negaMax(lNextStates.get(i), 0, -inf, inf, pDue);
            if(move > bestMove) {
                bestMove = move;
                index = i;
            }
        }
        return lNextStates.get(index);
    }

    public int analyseBoard(GameState gs) {
        if(gs.getMove().isEOG()) {
            if(gs.getMove().isRedWin() || gs.getMove().isWhiteWin()) {
                return 9001;
            }
        }

        int player;
        int enemy;

        if(gs.getNextPlayer() == Constants.CELL_WHITE) {
            player = Constants.CELL_WHITE;
            enemy = Constants.CELL_RED;
        } else {
            player = Constants.CELL_RED;
            enemy = Constants.CELL_WHITE;
        }

        int score = 0;
        // Count diff between white and red pieces.
        int myPieces = countPieces(gs, player);
        int enemyPieces = countPieces(gs, enemy);
        //score += positionValue(gs);
        score += (myPieces-enemyPieces);
        return score;
    }

    public int positionValue(GameState gs) {

        int score = 0;
        int val = 5;
        
        for(int i = 0; i < safe.length; i++) {
            if(gs.get(safe[i]) == Constants.CELL_WHITE) {
                score += val;
            }
        }
        
        return score;
    }
    public int countPieces(GameState board, int pType) {
        int pieces = 0;
        for(int i=0; i<GameState.NUMBER_OF_SQUARES; i++) {
            int cell = board.get(i);
            if(cell == pType) {
                pieces++;
                // Add points if piece is a king.
                if(cell == Constants.CELL_KING + pType)
                    pieces+=2;
            }
        }
        return pieces;
    }
    
    public int negaMax(GameState gs, int depth, int alpha, int beta, Deadline pDue) {
        if(gs.isEOG() || depth > maxDepth) {
            return analyseBoard(gs);
        }
        int max = -inf;

        Vector<GameState> gsvec = new Vector<GameState>();
        gs.findPossibleMoves(gsvec);
        for(int i=0; i<gsvec.size(); i++) {
            GameState tmp = new GameState(gs, gsvec.get(i).getMove());
            int x = -negaMax(tmp, depth+1, -beta, -alpha, pDue);
            if (x > max) {
                max = x;
            }
            if(x > alpha) {
                alpha = x;
            }
            if(alpha >= beta) {
                return alpha;
            }
        }
        return max;
    }
}
